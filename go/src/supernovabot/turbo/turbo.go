package turbo

import (
	"supernovabot/tools"
)

type Turbo struct {
	DurationMilliseconds, DurationTicks, Factor float64
}

func NewTurbo(durationMilliseconds, durationTicks, factor float64) *Turbo {
	return &Turbo{durationMilliseconds, durationTicks, factor}
}

func ParseTurbo(input interface{}) *Turbo {
	turbomap := tools.ConvItfToMap(input)
	durationMilliseconds := turbomap["turboDurationMilliseconds"].(float64)
	durationTicks := turbomap["turboDurationTicks"].(float64)
	factor := turbomap["turboFactor"].(float64)
	return NewTurbo(durationMilliseconds, durationTicks, factor)
}
