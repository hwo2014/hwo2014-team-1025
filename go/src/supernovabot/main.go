package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"supernovabot/ai"
	"supernovabot/carposition"
	"supernovabot/chrono"
	"supernovabot/status"
	"supernovabot/strategies"
	"supernovabot/targetspeed"
	"supernovabot/track"
	"supernovabot/turbo"
	"supernovabot/tools"
)

var BOTNAME string
var raceTrack track.Track
var targetspeeds *targetspeed.Targetspeeds
var prevStatus *status.Status
var strategy strategies.Strategy
var turbow *turbo.Turbo
var chronometer chrono.Chrono

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func read_msg(reader *bufio.Reader) (msg interface{}, err error) {
	var line string
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(line), &msg)
	if err != nil {
		return
	}
	return
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}, gametick float64) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	if gametick > 0 {
		m["gameTick"] = gametick
	}
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {
	data := make(map[string]string)
	data["name"] = name
	data["key"] = key
	err = write_msg(writer, "join", data, -1)
	return
}

func send_create_race(writer *bufio.Writer, name, key, trackname, password string, carcount float64) (err error) {
	data := make(map[string]interface{})
	botId := make(map[string]string)
	botId["name"] = name
	botId["key"] = key
	data["botId"] = botId
	data["trackName"] = trackname
	data["password"] = password
	data["carCount"] = carcount
	err = write_msg(writer, "createRace", data, -1)
	return
}

func send_join_race(writer *bufio.Writer, name, key, trackname, password string) (err error) {
	data := make(map[string]interface{})
	botId := make(map[string]string)
	botId["name"] = name
	botId["key"] = key
	data["botId"] = botId
	data["trackName"] = trackname
	data["password"] = password
	data["carCount"] = "1"
	err = write_msg(writer, "joinRace", data, -1)
	return
}

func send_ping(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "ping", make(map[string]string), -1)
	return
}

func send_switch(writer *bufio.Writer, direction string, gametick float64) (err error) {
	err = write_msg(writer, "switchLane", direction, gametick)
	return
}

func send_throttle(writer *bufio.Writer, throttle float32, gametick float64) (err error) {
	err = write_msg(writer, "throttle", throttle, gametick)
	return
}

func send_turbo(writer *bufio.Writer, message string, gametick float64) (err error) {
	err = write_msg(writer, "turbo", message, gametick)
	return
}

func dispatch_msg(writer *bufio.Writer, msgtype string, data interface{}, gameid string, gametick float64) (err error) {
	switch msgtype {
	case "join":
		log.Printf("Joined")
		send_ping(writer)
	case "gameStart":
		log.Printf("Game started")
		send_ping(writer)
	case "gameInit":
		log.Printf("Game initialized")
		raceTrack = track.BuildTrackFromData(data)
		if(targetspeeds == nil){
			targetspeeds = targetspeed.BuildTargetspeeds(raceTrack, .46)
			log.Printf("Targetspeeds UPDATED: %s", targetspeeds)
			chronometer = chrono.NewChrono()
		}
		ai.FuzzifyPieces(raceTrack.GetPieces())
		strategy = strategies.NewAlwaysTargetspeed(raceTrack, targetspeeds, chronometer)
		//strategy = strategies.NewAlwaysLeastTargetspeed(targetspeeds)
		//strategy = strategies.NewSlowAndSilly()
	case "crash":
		log.Printf("Someone crashed")
		datamap := tools.ConvItfToMap(data)
		var name string
		switch datamap["name"].(type) {
		case string:
			name = datamap["name"].(string)
		default :
			name = ""

		}
		if BOTNAME == name {
			targetspeeds.UpdateForce(prevStatus)
		}

		send_ping(writer)
	case "gameEnd":
		log.Printf("Game ended")
		send_ping(writer)
	case "carPositions":
		invoke_strategy(writer, data, gameid, gametick)
		//p, i := raceTrack.Get_next_bend()
		//fmt.Printf("Next bend is %s at position %s\n", p.String(), i)
		//switchToInnerLane(writer, p)
		//var trottle float32
		//trottle = 0.6
		//register_car_positions(data, gameid, gametick, trottle)
	case "turboAvailable":
		turbow = turbo.ParseTurbo(data)
		fmt.Printf("\n my turbo %s = ", turbow)
	case "error":
		send_ping(writer)
	default:
		log.Printf("Got msg type: %s", msgtype)
		send_ping(writer)
	}
	return
}

func invoke_strategy(writer *bufio.Writer, data interface{}, gameId string, gameTick float64) {
	log.Printf("------ STRATEGY tick: %d -----", int(gameTick))
	carspos := carposition.ParseCarpositionArray(data)
	mycarpos, myid := carposition.FindCar(carspos, BOTNAME)

	var competitorsStatuses = make(map[string]*status.Status)
	for id, compPos := range carspos {
		if id != myid {
			if prevStatus != nil {
				prevCompetitors := *prevStatus.Competitors
				competitorsStatuses[compPos.Id.Name] = status.NewStatus(gameId, gameTick, raceTrack, compPos, prevCompetitors[compPos.Id.Name], nil)
			} else {
				competitorsStatuses[compPos.Id.Name] = status.NewStatus(gameId, gameTick, raceTrack, compPos, nil, nil)
			}
		}
	}

	currentstatus := status.NewStatus(gameId, gameTick, raceTrack, mycarpos, prevStatus, &competitorsStatuses)
	log.Printf("status: %s", currentstatus)

	trottle, switchto, turbo := strategy.Run(*currentstatus, turbow)

	prevStatus = currentstatus

	log.Printf(" Status: %s", *currentstatus)

	// If you want to send a turbo or switchLane message,
	// you'll send that instead of the throttle message.
	if switchto != nil {
		send_switch(writer, *switchto, gameTick)
		log.Printf(" ==> SWITCH: %s", *switchto)
	} else {
		if turbo != nil {
			send_turbo(writer, *turbo, gameTick)
			log.Printf(" ==> TURBO: %s", *turbo)
			turbow = nil
		} else {
			send_throttle(writer, float32(trottle), gameTick)
			log.Printf(" ==> TROTTLE: %f", trottle)
		}
	}

	log.Printf("-----------------------------------------")

	// let's update the target speed in the background based on how we're doing right now
	targetspeeds.Update(*currentstatus)
	chronometer.Update(*currentstatus)

}

func parse_and_dispatch_input(writer *bufio.Writer, input interface{}) (err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool
		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}
		var gameid string
		var gametick float64

		gameid, ok = msg["gameId"].(string)
		gametick, ok = msg["gameTick"].(float64)

		switch msg["data"].(type) {
		default:
			err = dispatch_msg(writer, msg["msgType"].(string), nil, gameid, gametick)
			if err != nil {
				return
			}
		case interface{}:
			err = dispatch_msg(writer, msg["msgType"].(string), msg["data"].(interface{}), gameid, gametick)

			if err != nil {
				return
			}
		}
	}
	return
}

func bot_loop(conn net.Conn, name string, key string) (err error) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	send_join(writer, name, key)
	// comment out the send_join above and
	// replace with this send_create_race to race on another track
	//send_create_race(writer, name, key, "germany", "BelgianWaffels", 2)
	//send_join_race(writer, name, key, "germany", "BelgianWaffels")

	for {
		input, err := read_msg(reader)
		if err != nil {
			log_and_exit(err)
		}
		//log.Printf("Lets print for a while each incoming server msg: %s", input)
		err = parse_and_dispatch_input(writer, input)
		if err != nil {
			log_and_exit(err)
		}
	}
}

func parse_args() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func main() {
	host, port, name, key, err := parse_args()

	if err != nil {
		log_and_exit(err)
	}

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)
	BOTNAME = name

	conn, err := connect(host, port)

	if err != nil {
		log_and_exit(err)
	}

	defer conn.Close()

	err = bot_loop(conn, name, key)
}
