package status

import (
	"bytes"
	"fmt"
	"supernovabot/carposition"
	"supernovabot/track"
)

type Status struct {
	GameId              string
	Tick                float64
	Track               track.Track
	Speed, Acceleration float64
	Position            carposition.Carposition
	PrevStatus          *Status
	Competitors         *map[string]*Status
}

// TODO: fix memory eating bug (eternal status history Status->PrevStatus->PrevStatus->...)

func NewStatus(gameId string, tick float64, track track.Track, position carposition.Carposition, prevStatus *Status, competitors *map[string]*Status) *Status {
	var speed float64
	var acc float64

	if prevStatus == nil {
		speed = 0
	} else {
		speed = calculateSpeed(position, prevStatus.Position, track)
		acc = speed - prevStatus.Speed
	}

	return &Status{gameId, tick, track, speed, acc, position, prevStatus, competitors}
}

func calculateSpeed(current carposition.Carposition, prev carposition.Carposition, track track.Track) float64 {
	speed := current.PiecePosition.InPieceDistance - prev.PiecePosition.InPieceDistance

	if speed < 0 {
		prefDfc := track.GetLane(prev.PiecePosition.Lane.EndLaneIndex).GetDfc()
		prevsize := track.GetPieces()[int(prev.PiecePosition.PieceIndex)].Length(prefDfc)
		return (prevsize - prev.PiecePosition.InPieceDistance) + current.PiecePosition.InPieceDistance
	}

	return speed
}

// Look mummy, I have a toString method!
func (s Status) String() string {
	var buffer bytes.Buffer

	if s.Competitors != nil {
		buffer.WriteString(fmt.Sprintf(
			"\n    lap:  %d    piece:  %d - %f    lane:  %d -> %d    speed:  %f    acc: %f     drift: %f  comp:%d",
			int(s.Position.PiecePosition.Lap),
			int(s.Position.PiecePosition.PieceIndex),
			s.Position.PiecePosition.InPieceDistance,
			int(s.Position.PiecePosition.Lane.StartLaneIndex),
			int(s.Position.PiecePosition.Lane.EndLaneIndex),
			s.Speed,
			s.Acceleration,
			s.Position.Angle,
			len(*s.Competitors)))

		buffer.WriteString(string("\n"))

		buffer.WriteString(fmt.Sprintf("Competitors: %s", *s.Competitors))
	} else {
		buffer.WriteString(fmt.Sprintf(
			"    lap:  %d    piece:  %d - %f    lane:  %d -> %d    speed:  %f    acc: %f     drift: %f ",
			int(s.Position.PiecePosition.Lap),
			int(s.Position.PiecePosition.PieceIndex),
			s.Position.PiecePosition.InPieceDistance,
			int(s.Position.PiecePosition.Lane.StartLaneIndex),
			int(s.Position.PiecePosition.Lane.EndLaneIndex),
			s.Speed,
			s.Acceleration,
			s.Position.Angle))
	}

	return buffer.String()
}
