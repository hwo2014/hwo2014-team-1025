package ai

import (
	"bytes"
	"fmt"
	//	"strings"
	"supernovabot/track"
)

func GetFuzzyBends() []FuzzySet {

	var bends []FuzzySet

	/*
		file := ReadFile("tr1.fsd")
			lines := strings.Split(file, "\n")
				for line := range lines {
				    if strings.Contains(line, "bend")  {
						fuzzyset := CreateFuzzySet(line)
						bends = append(bends, fuzzyset)
					}
				}
	*/
	return bends
}

func FuzzifyPieces(pieces []track.Piece) {

	//fuzzybends := GetFuzzyBends()

	var buffer bytes.Buffer

	for i := range pieces {
		if pieces[i].AngleSigned() > 0 {
			buffer.WriteString(fmt.Sprintf("[%v]\n", pieces[i].Position()))
		}
	}

	WriteToFile("result.txt", []byte(buffer.String()))

}

func fuzzySetToString(fuzzyset FuzzySet) string {

	leftdegree0 := fmt.Sprintf("%v,%v", fuzzyset.Trapezoid.LeftDegree0.X, fuzzyset.Trapezoid.LeftDegree0.Y)
	leftdegree1 := fmt.Sprintf("%v,%v", fuzzyset.Trapezoid.LeftDegree1.X, fuzzyset.Trapezoid.LeftDegree1.Y)
	rightdegree0 := fmt.Sprintf("%v,%v", fuzzyset.Trapezoid.RightDegree0.X, fuzzyset.Trapezoid.RightDegree0.Y)
	rightdegree1 := fmt.Sprintf("%v,%v", fuzzyset.Trapezoid.RightDegree1.X, fuzzyset.Trapezoid.RightDegree1.Y)

	trapezoid := fmt.Sprintf("%v;%v;%v;%v", leftdegree0, leftdegree1, rightdegree0, rightdegree1)

	output := fmt.Sprintf("%v||%v||", fuzzyset.Name, trapezoid)

	return output

}
