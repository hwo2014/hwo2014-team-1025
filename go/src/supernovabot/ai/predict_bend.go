package ai

import ()

const (
	crash = 1
)

// Create a fuzzy set containing let's say 0..90 degrees
var degreeSet [90]int

// Create slices
type sliceDegreeSet struct {
	Length        int
	ZerothElement int
}

var sliceSharpTurn = sliceDegreeSet{25, 65}
