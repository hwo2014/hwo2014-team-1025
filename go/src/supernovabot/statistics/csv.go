package statistics

import (
	"encoding/csv"
	"os"
	"strconv"
)

type Csv struct {
	writer    csv.Writer
	firstline *bool
}

func NewCsv(filename string) Csv {
	file, _ := os.Create(filename)
	writer := csv.NewWriter(file)
	var first = new(bool)
	*first = true
	return Csv{*writer, first}
}

func (csv Csv) AddStat(lap, piece, posinpiece, lanestart, laneend, speed, acc, drift, targetspeed, trottle float64) {
	csv.writeTitles()
	var data = []string{
		csv.toStr(lap),
		csv.toStr(piece),
		csv.toStr(posinpiece),
		csv.toStr(lanestart),
		csv.toStr(laneend),
		csv.toStr(speed),
		csv.toStr(acc),
		csv.toStr(drift),
		csv.toStr(targetspeed),
		csv.toStr(trottle),
		csv.toStr(drift / 10),
		csv.toStr(trottle * 10)}
	csv.writer.Write(data)
}

func (csv Csv) toStr(f float64) string {
	return strconv.FormatFloat(f, 'f', 3, 64)
}

func (csv Csv) writeTitles() {
	if *csv.firstline {
		var data = []string{"lap", "piece", "inpiece", "laneS", "laneE", "speed", "acc", "drift", "targetspeed", "trottle", "driftnorm", "trotnorm"}
		csv.writer.Write(data)
		*csv.firstline = false
	}
}
