package track

import (
	"log"
	"supernovabot/direction"
	"supernovabot/tools"

)

type Track struct {
	pieces        []Piece
	lanes         []Lane
	LeftMostLane  Lane
	RightMostLane Lane
	numberOfLaps  int
}

func (t Track) GetPieces() []Piece {
	return t.pieces
}

func (t Track) GetLanes() []Lane {
	return t.lanes
}

func (t Track) GetLane(index float64) Lane {
	var lane Lane
	for _, lane = range t.GetLanes() {
		if lane.GetIndex() == index {
			break
		}
	}
	return lane
}

func (t Track) GetNumberOfLaps() int {
	return t.numberOfLaps
}

func BuildTrackFromData(data interface{}) (raceTrack Track) {

	datamap := tools.ConvItfToMap(data)
	race := tools.ConvItfToMap(datamap["race"])
	track := tools.ConvItfToMap(race["track"])

	piecesresult := parsePieces(track["pieces"])
	log.Printf("Track: pieces%s\n", piecesresult)

	lanesresult := parseLanes(track["lanes"])
	log.Printf("Track: lanes%s\n", lanesresult)

	numberoflaps := parseRaceSession(race["raceSession"])
	log.Printf("Number of laps %s\n", numberoflaps)

	return Track{piecesresult, lanesresult, getLeftMostLane(lanesresult), getRightMostLane(lanesresult), numberoflaps}
}

func (t Track) IsLefMostLane(l Lane) bool {
	var isLeft = false
	var val Lane
	for _, val = range t.lanes {
		if l.dfc < val.dfc {
			isLeft = true
		} else {
			isLeft = false
		}
	}
	return isLeft
}
func (t Track) GetLaneInDirection(lane Lane, dir int) Lane {
	var found *Lane = nil
	for _, val := range t.GetLanes() {
		switch {
		case dir == direction.LEFT:
			if val.dfc < lane.dfc && (found == nil || val.dfc > found.dfc) {
				found = &val
			}
		case dir == direction.RIGHT:
			if val.dfc > lane.dfc && (found == nil || val.dfc < found.dfc) {
				found = &val
			}
		}
	}

	if found == nil {
		return lane
	} else {
		return *found
	}
}

func getLeftMostLane(lanes []Lane) Lane {
	found := lanes[0]
	for _, val := range lanes {
		if val.dfc < found.dfc {
			found = val
		}
	}

	return found
}

func getRightMostLane(lanes []Lane) Lane {
	found := lanes[0]
	for _, val := range lanes {
		if val.dfc > found.dfc {
			found = val
		}
	}

	return found
}

func (t Track) IsRigthMostLane(l Lane) bool {
	var isRight = false
	var val Lane
	for _, val = range t.lanes {
		if l.dfc > val.dfc {
			isRight = true
		} else {
			isRight = false
		}
	}
	return isRight
}

func (t Track) Get_next_bend(pieceIndex int) (Piece, int) {
	var key int
	var piece Piece
	for key, piece = range t.pieces {
		if piece.Bend() && piece.Position() >= pieceIndex {
			break
		}
	}
	return piece, key
}

func (t Track) Get_last_bend() (int){
	var key int
	for key = range t.pieces {
		if t.pieces[len(t.pieces)-1-key].Bend()  {
			break
		}
	}
	return len(t.pieces)-1-key
}

func (t Track) Get_next_switch(pieceIndex int) (Piece, int) {
	var key int
	var piece Piece
	for key, piece = range t.pieces {
		if piece.Switch() && piece.Position() >= pieceIndex {
			break
		}
	}
	return piece, key
}

func parsePieces(input interface{}) (piecesresult []Piece) {
	pieces := tools.ConvItfToArray(input)

	//piecesresult := make([]Piece, 0)

	for key, val := range pieces {
		piece := tools.ConvItfToMap(val)

		var length, radius, angle float64
		var switcher bool

		// TODO: clean this mess up, I hate safe parsing

		switch piece["length"].(type) {
		case float64:
			length = piece["length"].(float64)
		}

		switch piece["radius"].(type) {
		case float64:
			radius = piece["radius"].(float64)
		}

		switch piece["angle"].(type) {
		case float64:
			angle = piece["angle"].(float64)
		}

		switch piece["switch"].(type) {
		case bool:
			switcher = piece["switch"].(bool)
		}

		np := NewPiece(length, radius, angle, switcher, key)

		//log.Printf("Piece %d:  %s", np)

		piecesresult = append(piecesresult, np)
	}
	return piecesresult
}

func parseLanes(input interface{}) (lanesresult []Lane) {
	lanes := tools.ConvItfToArray(input)

	for _, val := range lanes {
		var index, dfc float64

		lane := tools.ConvItfToMap(val)

		switch lane["index"].(type) {
		case float64:
			index = lane["index"].(float64)
		}

		switch lane["distanceFromCenter"].(type) {
		case float64:
			dfc = lane["distanceFromCenter"].(float64)
		}

		nl := NewLane(index, dfc)

		lanesresult = append(lanesresult, nl)
	}
	return lanesresult
}

func parseRaceSession(input interface{}) (int) {
	raceSession := tools.ConvItfToMap(input)
	var laps int
	switch raceSession["laps"].(type) {
	case float64:
		laps = int(raceSession["laps"].(float64))
	default :
		laps = -1

	}
	return laps
}
