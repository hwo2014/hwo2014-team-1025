package track

import (
	"fmt"
)

type Lane struct {
	index, dfc float64
}

// constructor for lane
func NewLane(index, dfc float64) Lane {
	return Lane{index, dfc}
}

func (l Lane) GetIndex() float64 {
	return l.index
}

func (l Lane) GetDfc() float64 {
	return l.dfc
}

// Look mummy, I have a toString method!
func (l Lane) String() string {
	return fmt.Sprintf("{ index: %f dfc: %f }", l.index, l.dfc)
}
