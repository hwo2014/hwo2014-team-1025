package track

import (
	"fmt"
	"math"
)

const MAXDEGREE = float64(360)

type Piece struct {
	_length, _radius, _angle float64
	_switch                  bool
	position                 int
}

// constructor for Piece
func NewPiece(length, radius, angle float64, switcher bool, position int) Piece {
	return Piece{length, radius, angle, switcher, position}
}

func (p Piece) Position() int {
	return p.position
}
func (p Piece) Straight() bool {
	// if piece rotation is 0 degrees, piece is straight
	return p.AngleSigned() == 0
}

func (p Piece) Bend() bool {
	// non-straight pieces are bended
	return !p.Straight()
}

func (p Piece) Length(distancefromcenter float64) float64 {
	if p.Straight() {
		return p._length
	} else {
		// calculate bend length based on angle and radius
		// first calculate circle circumference, then arc size
		radius := p.LaneRadius(distancefromcenter)
		circumference := 2 * radius * math.Pi
		arc := (p.AngleUnsigned() / MAXDEGREE) * circumference

		//log.Printf("Length -> dfc: %f radius: %f  arc: %f ", distancefromcenter, radius, arc)
		return arc
	}
}

func (p Piece) LaneRadius(dfc float64) float64 {
	if p.AngleSigned() == 0 {
		return 0
	}
	if p.AngleSigned() > 0 {
		return p._radius - dfc
	} else {
		return p._radius + dfc
	}
}

func (p Piece) AngleUnsigned() float64 {
	return math.Abs(p.AngleSigned())
}

func (p Piece) AngleSigned() float64 {
	return p._angle
}

func (p Piece) BendRight() bool {
	if p.Straight() {
		return false
	} else {
		return p.AngleSigned() > 0
	}
}

func (p Piece) BendLeft() bool {
	if p.Straight() {
		return false
	} else {
		return p.AngleSigned() < 0
	}
}

func (p Piece) Switch() bool {
	return p._switch
}

func (p Piece) Radius() float64 {
	return p._radius
}

// Look mummy, I have a toString method!
func (p Piece) String() string {
	return fmt.Sprintf("{ len: %f rad: %f ang: %f switch: %t }", p.Length(0), p.Radius(), p.AngleSigned(), p.Switch())
}
