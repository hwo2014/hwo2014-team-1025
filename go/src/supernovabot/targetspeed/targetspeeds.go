package targetspeed

import (
	"fmt"
	"log"
	"math"
	"supernovabot/status"
	"supernovabot/track"
)

const (
	// decceleration for 100 distance units
	DeccFactor = 2.2
)

type Targetspeeds struct {
	targets []Target
	update  chan status.Status
	trackinfo track.Track
	maxforce float64
}


func (t *Targetspeeds) SetMaxForce(force float64) {
	t.maxforce = force
}

func (t Targetspeeds) GetMaxForce() float64 {
	return t.maxforce
}

// constructor Targetspeeds
func newTargetspeeds(targets []Target, trackinfo track.Track, maxforce float64) *Targetspeeds {
	update := make(chan status.Status)
	speeds := Targetspeeds{targets, update, trackinfo, maxforce}

	go func() {
		currentpiece := -1
		maxangle := 0.00
		avgspeed := 0.00
		tickamount := 0.00
		drifting := false
		for {
			select {
			case status := <-update:
				piece := int(status.Position.PiecePosition.PieceIndex)
				pieceStruct := trackinfo.GetPieces()[int(status.Position.PiecePosition.PieceIndex)]
				lane := status.Position.PiecePosition.Lane.StartLaneIndex
				angle := math.Abs(status.Position.Angle)

				if !drifting && angle > 55 && pieceStruct.Bend() {
					currenttarget := speeds.GetTargetSpeed(piece, lane)
					radius := pieceStruct.LaneRadius(lane)
					newForce := (status.Speed) * (status.Speed) / radius
					log.Printf("Angle > 55, recalculating targetspeeds based on current speed %f instead of target speed %f", status.Speed, currenttarget)
					log.Printf("Radius for piece %d, lane %f is %f", piece, lane, radius)
					log.Printf("New maximum force is now at %f instead of %f", newForce, speeds.maxforce)
					finalForce := math.Min(newForce, speeds.maxforce)
					speeds.RecalculateTargetSpeeds(finalForce)
					speeds.SetMaxForce(finalForce)
					log.Printf("I recalculated targetspeeds with force %f", speeds.maxforce)
					drifting = true
					if angle < 55 && drifting {
						drifting = false
					}
				}
				if currentpiece < 0 {
					currentpiece = piece
				}

				if currentpiece != piece {
					// looks like we moved on to the next piece
					// let's process what we learned from this
					fmt.Printf(">>> The maximum angle we achieved in piece %d (lane %f) was %f°\n", currentpiece, lane, maxangle)
					fmt.Printf(">>> The average speed we achieved in piece %d (lane %f) was %f\n", currentpiece, lane, avgspeed)
					fmt.Println()

					currenttarget := speeds.GetTargetSpeed(currentpiece, lane)
					if avgspeed > currenttarget*0.90 {
						if maxangle < 40 && status.Track.GetPieces()[currentpiece].Bend() {
							newtarget := currenttarget * 1.02
							// let's do a bit more here
							if maxangle < 20 {
								newtarget = currenttarget * 1.05
							}
							fmt.Printf("   Let's turn it up one notch - from %f to %f", currenttarget, newtarget)
							fmt.Println()
							speeds.SetTargetSpeed(currentpiece, lane, newtarget)
						}
					}

					// and now, move on to the next one
					currentpiece = piece
					avgspeed = status.Speed
					maxangle = 0
					tickamount = 0
				}
				tickamount++
				avgspeed = (avgspeed*tickamount + status.Speed) / (tickamount + 1)
				maxangle = math.Max(maxangle, angle)
			}
		}
	}()
	return &speeds
}

func (t Targetspeeds) GetTargetSpeed(pieceindex int, laneindex float64) float64 {
	targetspeed, _ := getTargetSpeed(t.targets, pieceindex, laneindex)
	return targetspeed
}

func getTargetSpeed(targets []Target, pieceindex int, laneindex float64) (float64, int) {
	for id, target := range targets {
		if target.pieceindex == pieceindex && target.laneindex == laneindex {
			return target.targetspeed, id
		}
	}
	return -1, -1
}

func (t *Targetspeeds) SetTargetSpeed(pieceindex int, laneindex, targetspeed float64) {
	found := false
	for i, target := range t.targets {
		if target.pieceindex == pieceindex && target.laneindex == laneindex {
			t.targets[i].targetspeed = targetspeed
			found = true
		}
	}

	if found == false {
		t.targets = append(t.targets, NewTarget(pieceindex, laneindex, targetspeed))
	}
}

func (t Targetspeeds) Update(status status.Status) {
	t.update <- status
}

func BuildTargetspeeds(trackinfo track.Track, maxforce float64) *Targetspeeds {
	var targets []Target
	result := newTargetspeeds(targets, trackinfo, maxforce)
	result.RecalculateTargetSpeeds(maxforce)
	log.Printf("Targetspeeds: %s", result)
	return result
}

func (t *Targetspeeds) RecalculateTargetSpeeds(maxforce float64) {
	// get Pieces
	pieces := t.trackinfo.GetPieces()

	// get al pieces en find bends (only bends have max speed)
	for index, piece := range pieces {
		if piece.Bend() {
			// calculate target speeds for each lane of the bend
			calculateTargets(&t.targets, t.trackinfo, index, piece, t.trackinfo.GetLanes(), maxforce)
			log.Printf("DEBUG: BuildTargetspeeds: %s", t.targets)
		}
	}

	t.AddDeccelerationSpeed(t.trackinfo)
}

func(t *Targetspeeds) UpdateForce(status *status.Status) {

	var found = false
	for found {
		if status.Speed != 0 {
			status = status.PrevStatus
		}else{
			found = true
		}
	}
	//only update if we crashed because of roadhogs or because we turbo'ed out
	if status.Speed < t.GetTargetSpeed(int(status.Position.PiecePosition.PieceIndex), status.Position.PiecePosition.Lane.StartLaneIndex) {
		log.Printf("\n ==== Updating Force ==== \n")
		t.SetMaxForce((status.Speed) * (status.Speed) / t.trackinfo.GetPieces()[int(status.Position.PiecePosition.PieceIndex)].LaneRadius(status.Position.PiecePosition.Lane.StartLaneIndex))
		t.RecalculateTargetSpeeds(t.GetMaxForce())
		log.Printf("new force is %s", t.GetMaxForce())
	}
}

// inverse formula of centripetal acceleration
// sideways force = speed^2 / radius
// -> speed = sqrt(force*radius)
func maxspeed(maxforce, radius float64) float64 {
	return math.Sqrt(maxforce * radius)
}

func calculateTargets(targets *[]Target, trackinfo track.Track, index int, piece track.Piece, lanes []track.Lane, maxforce float64) {
	for _, lane := range lanes {

		// calculate lane radius (with distance from center value in lane)
		radius := piece.LaneRadius(lane.GetDfc())

		// calculate maxspeed
		speed := maxspeed(maxforce, radius)

		addTargetspeedIfSmaller(targets, index, lane.GetIndex(), speed)
		//also set target speed for next late to get out of bend alive.
		addTargetspeedIfSmaller(targets, int(math.Mod(float64(index+1), float64(len(trackinfo.GetPieces())))), lane.GetIndex(), speed)
	}
	log.Printf("DEBUG: calculateTargets: %s", targets)

}

func addTargetspeedIfSmaller(targets *[]Target, pieceindex int, laneindex float64, targetspeed float64) {
	oldTarget, id := getTargetSpeed(*targets, pieceindex, laneindex)
	if oldTarget == -1 {
		*targets = append(*targets, NewTarget(pieceindex, laneindex, targetspeed))
	} else {
		if oldTarget > targetspeed {
			tval := *targets
			tval[id].targetspeed = targetspeed
		}
	}

	log.Printf("DEBUG: addTargetspeedIfSmaller: %s", *targets)
}

func (t *Targetspeeds) AddDeccelerationSpeed(track track.Track) {
	for id, piece := range track.GetPieces() {
		for _, lane := range track.GetLanes() {
			if piece.Bend() {
				fmt.Print("\n")
				backwardsDeccelarationSpeed(id-1, lane, t.GetTargetSpeed(id, lane.GetIndex()), t, track)
			}
		}
	}

}

func backwardsDeccelarationSpeed(pieceid int, lane track.Lane, targetspeed float64, targetspeeds *Targetspeeds, track track.Track) {
	var pid int
	if pieceid < 0 {
		pid = pieceid + len(track.GetPieces())
	} else {
		pid = pieceid
	}
	deccel := (DeccFactor * track.GetPieces()[pid].Length(lane.GetDfc())) / 100
	newTargetspeed := targetspeed + (deccel)
	oldTargetspeed := targetspeeds.GetTargetSpeed(pid, lane.GetIndex())
	fmt.Printf(" ->pid: %d os: %f  ns: %f   deccel: %f", pid, oldTargetspeed, newTargetspeed, deccel)

	if oldTargetspeed < 0 || oldTargetspeed > newTargetspeed {
		targetspeeds.SetTargetSpeed(pid, lane.GetIndex(), newTargetspeed)
		fmt.Printf(" W ")
	}
	if newTargetspeed < 50 {
		backwardsDeccelarationSpeed(pid-1, lane, newTargetspeed, targetspeeds, track)
	}
}

// Look mummy, I have a toString method!
func (t Targetspeeds) String() string {
	return fmt.Sprintf("Targetspeeds: %s", t.targets)
}
