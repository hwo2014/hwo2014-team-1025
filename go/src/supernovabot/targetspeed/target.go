package targetspeed

import (
	"fmt"
)

type Target struct {
	pieceindex             int
	laneindex, targetspeed float64
}

func NewTarget(pieceindex int, laneindex, targetspeed float64) Target {
	return Target{pieceindex, laneindex, targetspeed}
}

// Look mummy, I have a toString method!
func (t Target) String() string {
	return fmt.Sprintf("{ p:%d l:%f s:%f}", t.pieceindex, t.laneindex, t.targetspeed)
}
