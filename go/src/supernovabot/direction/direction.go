package direction

const (
	NONE  int = 0
	LEFT  int = 1
	RIGHT int = 2
)

func ToString(dir int) *string {
	var direction *string = nil
	switch {
	case dir == LEFT:
		dir := "Left"
		direction = &dir
	case dir == RIGHT:
		dir := "Right"
		direction = &dir
	}

	return direction
}
