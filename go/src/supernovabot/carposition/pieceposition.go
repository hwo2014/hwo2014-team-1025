package carposition

import (
	"supernovabot/tools"
)

type PiecePosition struct {
	PieceIndex, InPieceDistance, Lap float64
	Lane                             Lane
}

func NewPieceposition(pieceIndex, inPieceDistance, lap float64, lane Lane) PiecePosition {
	return PiecePosition{pieceIndex, inPieceDistance, lap, lane}
}

func ParsePieceposition(input interface{}) PiecePosition {
	ppmap := tools.ConvItfToMap(input)
	lane := ParseLane(ppmap["lane"])
	return NewPieceposition(ppmap["pieceIndex"].(float64), ppmap["inPieceDistance"].(float64), ppmap["lap"].(float64), lane)
}
