package carposition

import (
	"log"
	"supernovabot/tools"
)

type Carposition struct {
	Id            Id
	Angle         float64
	PiecePosition PiecePosition
}

func NewCarposition(id Id, angle float64, piecePosition PiecePosition) Carposition {
	return Carposition{id, angle, piecePosition}
}

func ParseCarposition(input interface{}) Carposition {
	cpmap := tools.ConvItfToMap(input)
	id := ParseId(cpmap["id"])
	angle := cpmap["angle"].(float64)
	piecePosition := ParsePieceposition(cpmap["piecePosition"])
	return NewCarposition(id, angle, piecePosition)
}

func ParseCarpositionArray(input interface{}) (carpositions []Carposition) {
	arr := tools.ConvItfToArray(input)
	for _, val := range arr {
		carpositions = append(carpositions, ParseCarposition(val))
	}
	return
}

func FindCar(positions []Carposition, name string) (Carposition, int) {
	for id, val := range positions {
		if val.Id.Name == name {
			return val, id
		}
	}
	log.Printf("ERROR: could not find car %s in positions %s", name, positions)
	return Carposition{}, -1
}
