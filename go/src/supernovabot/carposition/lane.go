package carposition

import (
	"supernovabot/tools"
)

type Lane struct {
	StartLaneIndex, EndLaneIndex float64
}

func NewLane(StartLaneIndex, EndLaneIndex float64) Lane {
	return Lane{StartLaneIndex, EndLaneIndex}
}

func ParseLane(input interface{}) Lane {
	lanemap := tools.ConvItfToMap(input)
	return NewLane(lanemap["startLaneIndex"].(float64), lanemap["endLaneIndex"].(float64))
}
