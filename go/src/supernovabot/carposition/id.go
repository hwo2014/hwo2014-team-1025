package carposition

import (
	"supernovabot/tools"
)

type Id struct {
	Name, Color string
}

func NewId(name, color string) Id {
	return Id{name, color}
}

func ParseId(input interface{}) Id {
	idmap := tools.ConvItfToMap(input)
	return NewId(idmap["name"].(string), idmap["color"].(string))
}
