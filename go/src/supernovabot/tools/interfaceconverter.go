package tools

import (
	"log"
)

func ConvItfToMap(input interface{}) map[string]interface{} {
	if isItfMap(input) {
		return input.(map[string]interface{})
	} else {
		log.Printf("ERROR: Conversion of %s to map failed", input)
		return nil
	}
}

func isItfMap(input interface{}) bool {
	switch input.(type) {
	case map[string]interface{}:
		return true
	default:
		return false
	}

}

func ConvItfToArray(input interface{}) []interface{} {
	if isItfArr(input) {
		return input.([]interface{})
	} else {
		log.Printf("ERROR: Conversion of %s to array failed", input)
		return nil
	}
}

func isItfArr(input interface{}) bool {
	switch input.(type) {
	case []interface{}:
		return true
	default:
		return false
	}

}
