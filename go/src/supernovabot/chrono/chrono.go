package chrono

import (
	"log"
	"supernovabot/status"
)

const (
	NO_VALUE = -1
)

type Chrono struct {
	timings map[key]float64
	updates chan status.Status
}

type key struct {
	piece, lane int
}

func NewChrono() Chrono {
	updates := make(chan status.Status)
	chrono := Chrono{make(map[key]float64), updates}

	go func() {
		currentpiece := -1
		currentlane := -1

		firstTick := -1
		firstDistance := -1.00
		lastTick := -1
		lastDistance := -1.00

		for {
			select {
			case status := <-updates:
				if int(status.Position.PiecePosition.Lap) < 1 {
					break
				}
				piece := int(status.Position.PiecePosition.PieceIndex)
				lane := int(status.Position.PiecePosition.Lane.StartLaneIndex)
				distance := status.Position.PiecePosition.InPieceDistance

				if currentpiece < 0 {
					firstTick = int(status.Tick)
					firstDistance = distance
					currentpiece = piece
					currentlane = lane
				}
				if piece == currentpiece {
					lastTick = int(status.Tick)
					lastDistance = distance
				} else {
					chrono.Set(currentpiece, currentlane, lastTick-firstTick, lastDistance-firstDistance)

					currentpiece = piece
					currentlane = lane
					firstTick = int(status.Tick)
					firstDistance = distance
					lastTick = int(status.Tick)
					lastDistance = distance
				}
			}
		}
	}()

	return chrono
}

func (chrono Chrono) Update(status status.Status) {
	chrono.updates <- status
}

func (chrono *Chrono) Set(pieceindex, laneindex, ticks int, distance float64) {
	speed := distance / float64(ticks)

	log.Printf(" CHRONO: it took %d ticks to travel %f distance in piece %d, lane %d", ticks, distance, pieceindex, laneindex)
	log.Printf("         at a speed of %f", speed)

	chrono.timings[key{pieceindex, laneindex}] = distance / float64(ticks)
}

func (chrono Chrono) Get(pieceindex, laneindex int) float64 {
	result, found := chrono.timings[key{pieceindex, laneindex}]
	if found {
		return result
	} else {
		return NO_VALUE
	}
}
