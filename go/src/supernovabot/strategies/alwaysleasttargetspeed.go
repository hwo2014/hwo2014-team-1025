package strategies

import (
	"log"
	"supernovabot/statistics"
	"supernovabot/status"
	"supernovabot/strategies/trottle"
	"supernovabot/targetspeed"
	"supernovabot/turbo"
)

type AlwaysLeastTargetspeed struct {
	targetspeeds     targetspeed.Targetspeeds
	leastTargetSpeed *float64
	csv              statistics.Csv
}

func NewAlwaysLeastTargetspeed(targetspeeds targetspeed.Targetspeeds) AlwaysLeastTargetspeed {
	return AlwaysLeastTargetspeed{targetspeeds, new(float64), statistics.NewCsv("AlwaysLeastTargetspeed.csv")}
}

func (s AlwaysLeastTargetspeed) Run(status status.Status, turbo *turbo.Turbo) (float64, *string, *string) {

	if *s.leastTargetSpeed == 0 {
		pieceAmount := len(status.Track.GetPieces())

		*s.leastTargetSpeed = 10

		for i := 0; i < pieceAmount; i++ {
			targetspeed := s.targetspeeds.GetTargetSpeed(int(i), status.Position.PiecePosition.Lane.StartLaneIndex)

			if targetspeed != -1 && targetspeed < *s.leastTargetSpeed {
				*s.leastTargetSpeed = targetspeed
			}
		}
	}

	log.Printf(" leastTargetSpeed: %f", *s.leastTargetSpeed)

	trottle := trottle.ToTargetSpeedPid(status.Speed, *s.leastTargetSpeed)

	writeStat(s.csv, status, *s.leastTargetSpeed, trottle)
	return trottle, nil, nil
}
