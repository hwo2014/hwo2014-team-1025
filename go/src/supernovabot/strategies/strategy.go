package strategies

import (
	"supernovabot/status"
	"supernovabot/turbo"
)

type Strategy interface {
	// return trottle, switch (nil if none), turbo (nil if none)
	Run(status.Status, *turbo.Turbo) (float64, *string, *string)
}
