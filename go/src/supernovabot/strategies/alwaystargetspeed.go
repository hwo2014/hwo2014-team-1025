package strategies

import (
	"log"
	"math"
	"supernovabot/chrono"
	"supernovabot/direction"
	"supernovabot/statistics"
	"supernovabot/status"
	"supernovabot/strategies/lanes"
	"supernovabot/strategies/switcher"
	"supernovabot/strategies/trottle"
	"supernovabot/strategies/turbolauncher"
	"supernovabot/targetspeed"
	"supernovabot/track"
	"supernovabot/turbo"
)

type AlwaysTargetspeed struct {
	raceTrack           track.Track
	targetspeeds        *targetspeed.Targetspeeds
	lastSwitchCalcPiece *float64
	nexSwitchDirection  *int
	csv                 statistics.Csv
	lanepicker          lanes.Picker
	turbopiece          *int
	prevTargetspeed     *float64
	targetspeedDiff     *float64
}

func NewAlwaysTargetspeed(track track.Track, targetspeeds *targetspeed.Targetspeeds, chronometer chrono.Chrono) AlwaysTargetspeed {
	var zero = new(float64)
	*zero = 0

	var negative = new(float64)
	*negative = -1

	var none = new(int)
	*none = 0
	return AlwaysTargetspeed{track, targetspeeds, negative, none, statistics.NewCsv("AlwaysTargetspeed.csv"), lanes.NewPicker(track, targetspeeds, chronometer), nil, new(float64), new(float64)}
}

func (s AlwaysTargetspeed) Run(status status.Status, turbo *turbo.Turbo) (float64, *string, *string) {
	if status.PrevStatus == nil {
		return 1, nil, nil
	}

	var switchstring *string = nil

	// calculate switch upon entering each new piece
	if *s.lastSwitchCalcPiece != status.Position.PiecePosition.PieceIndex {
		//switchPiece, switchDirection := s.lanepicker.GetNextSwitchDirection(int(status.Position.PiecePosition.PieceIndex), status.Position.PiecePosition.Lane.EndLaneIndex)
		switchPiece, switchDirection := lanes.RouteGetNextSwitch(status.Position.PiecePosition.PieceIndex, status.Track.GetLane(status.Position.PiecePosition.Lane.EndLaneIndex), status.Track, s.targetspeeds, *status.Competitors)
		// set nexSwitchDirection to ask right targetspeed
		*s.nexSwitchDirection = switchDirection
		// only actually send switch once, one piece before the actual switch
		if int(status.Position.PiecePosition.PieceIndex) == (switchPiece - 1) {
			switchstring = direction.ToString(switchDirection)
		}
		*s.lastSwitchCalcPiece = status.Position.PiecePosition.PieceIndex
	}

	if turbo != nil {
		//OMG, we got turbo!
		s.turbopiece = new(int)
		//if this is the last lap use turbo before finishline
		if s.raceTrack.GetNumberOfLaps() != -1 && s.raceTrack.GetNumberOfLaps()-1 == int(status.Position.PiecePosition.Lap) && int(status.Position.PiecePosition.PieceIndex) >= s.raceTrack.Get_last_bend()-1 {
			*s.turbopiece = s.raceTrack.Get_last_bend() + 1
			log.Printf("LAST LAP!! Turbo charged, target set for piece %d", *s.turbopiece)
		} else {
			*s.turbopiece, _ = turbolauncher.LongestSprint(status, s.targetspeeds, *turbo)
			log.Printf("Turbo charged, target set for piece %d", *s.turbopiece)
		}

	}

	targetSpeed := s.getTargetspeed(status, *s.nexSwitchDirection)

	log.Printf(" curr: %f - target: %f", status.Speed, targetSpeed)

	trottle := trottle.ToTargetSpeedPid(status.Speed, targetSpeed)
	/*
		// a little touch of ESP
		if math.Abs(status.Position.Angle) > 57 {
			trottle = 0
		}*/

	writeStat(s.csv, status, targetSpeed, trottle)

	return trottle, switchstring, s.canigoturbo(status)
	//return trottle, nil, nil
}

func (s AlwaysTargetspeed) getTargetspeed(status status.Status, dir int) float64 {
	pieceAmount := len(status.Track.GetPieces())
	targetspeed := float64(-1)
	pieceIndex := status.Position.PiecePosition.PieceIndex
	_, nextBendIndex := status.Track.Get_next_bend(int(pieceIndex))
	_, nexSwitchIndex := status.Track.Get_next_switch(int(pieceIndex))

	var laneIndex float64
	lane := status.Track.GetLane(status.Position.PiecePosition.Lane.StartLaneIndex)
	if nexSwitchIndex <= nextBendIndex {
		laneIndex = status.Track.GetLaneInDirection(lane, *s.nexSwitchDirection).GetIndex()
	} else {
		laneIndex = status.Position.PiecePosition.Lane.StartLaneIndex
	}
	var foundPieceId int

	for i := pieceIndex; targetspeed == -1; {
		foundPieceId = int(i) + 1
		targetspeed = s.targetspeeds.GetTargetSpeed(foundPieceId, laneIndex)
		i++
		i = math.Mod(i, float64(pieceAmount))
	}

	// get interpolated targetspeed
	/*
		prevTargetSpeed := s.targetspeeds.GetTargetSpeed(foundPieceId-1, laneIndex)
		if prevTargetSpeed == -1 {
			return targetspeed
		} else {
			piecepercentage := status.Position.PiecePosition.InPieceDistance / status.Track.GetPieces()[int(pieceIndex)].Length(lane.GetDfc())
			return targetspeed + (targetspeed-prevTargetSpeed)*piecepercentage
		}*/

	return s.postponeDeccel(status, lane, targetspeed)
	//return targetspeed

}

func (s AlwaysTargetspeed) postponeDeccel(status status.Status, lane track.Lane, targetsp float64) float64 {
	if *s.prevTargetspeed != targetsp {
		*s.targetspeedDiff = targetsp - *s.prevTargetspeed
	}

	if *s.targetspeedDiff < 0 {
		// calculate remaining distance and distance needed to deccelerate, do not decellerate until needed

		remaining := status.Track.GetPieces()[int(status.Position.PiecePosition.PieceIndex)].Length(lane.GetDfc()) - status.Position.PiecePosition.InPieceDistance
		needed := targetspeed.DeccFactor / 100 * *s.targetspeedDiff

		if remaining > needed {
			return *s.prevTargetspeed
		}
	}

	return targetsp

}

func (s AlwaysTargetspeed) calculateSwitch(status status.Status) *string {
	if *s.lastSwitchCalcPiece != status.Position.PiecePosition.PieceIndex {
		*s.lastSwitchCalcPiece = float64(status.Position.PiecePosition.PieceIndex)
		return direction.ToString(switcher.AlwaysInside(status))
	} else {
		return nil
	}
}

func (s AlwaysTargetspeed) canigoturbo(status status.Status) *string {
	if s.turbopiece != nil && float64(*s.turbopiece) == status.Position.PiecePosition.PieceIndex {
		s.turbopiece = nil

		var message = new(string)
		*message = "GIVE ME BACK MY COOKIIEEEEE !!!"
		return message

	} else {
		return nil
	}

}

func writeStat(csv statistics.Csv, st status.Status, targetSpeed, trottle float64) {
	// AddStat(lap, piece, posinpiece, lanestart, laneend, speed, acc, drift, targetspeed, trottle float64) {
	csv.AddStat(
		st.Position.PiecePosition.Lap,
		st.Position.PiecePosition.PieceIndex,
		st.Position.PiecePosition.InPieceDistance,
		st.Position.PiecePosition.Lane.StartLaneIndex,
		st.Position.PiecePosition.Lane.EndLaneIndex,
		st.Speed,
		st.Acceleration,
		st.Position.Angle,
		targetSpeed,
		trottle)

}
