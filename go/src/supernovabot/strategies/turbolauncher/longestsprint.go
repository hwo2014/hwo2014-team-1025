package turbolauncher

import (
	"math"
	"supernovabot/status"
	"supernovabot/targetspeed"
	"supernovabot/turbo"
)

func LongestSprint(status status.Status, targetspeed *targetspeed.Targetspeeds, turbo turbo.Turbo) (pieceid, piecesprintLength int) {

	// find longest sprint size for each piece, return piece with longest
	pieceamount := len(status.Track.GetPieces())
	for i := 0; i < pieceamount; i++ {
		length := findSprintLength(i, status, targetspeed)
		if length > piecesprintLength {
			pieceid = i
			piecesprintLength = length
		}
	}
	return

}

func findSprintLength(pieceid int, status status.Status, targetspeed *targetspeed.Targetspeeds) (length int) {
	pieceamount := len(status.Track.GetPieces())
	for i := 0; i < pieceamount; i++ {
		var higher bool = true
		// all lanes have to be higher than the threshold
		for _, lane := range status.Track.GetLanes() {
			targesp := targetspeed.GetTargetSpeed(int(math.Mod(float64(pieceid+i), float64(pieceamount))), lane.GetIndex())
			if targesp > 0 && targesp < 10 {
				higher = false
			}
		}

		if higher {
			length = length + 1
		} else {
			break
		}

	}

	return

}
