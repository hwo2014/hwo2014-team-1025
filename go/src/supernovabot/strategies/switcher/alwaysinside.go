package switcher

import (
	"supernovabot/direction"
	"supernovabot/status"
)

func AlwaysInside(status status.Status) int {
	nextBend, _ := status.Track.Get_next_bend(int(status.Position.PiecePosition.PieceIndex))
	raceTrack := status.Track
	currentLaneIndex := status.Position.PiecePosition.Lane.EndLaneIndex

	if nextBend.BendLeft() && raceTrack.LeftMostLane.GetIndex() != currentLaneIndex {
		return direction.LEFT
	}
	if nextBend.BendRight() && raceTrack.RightMostLane.GetIndex() != currentLaneIndex {
		return direction.RIGHT
	}

	return direction.NONE
}
