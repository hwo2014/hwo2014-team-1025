package trottle

import (
	"log"
)

const (
	kp                float64 = 85
	ki                float64 = 0
	kd                float64 = 0.0001
	st                float64 = 1
	stcoeff           float64 = 0.1
	strangecorrection float64 = 0.02
)

var errsum float64 = 0
var lasterr float64 = 0
var lasttargetspeed float64 = -1

func ToTargetSpeedPid(currentSpeed float64, targetSpeed float64) float64 {

	detectNewTargetspeed(targetSpeed)

	err := (targetSpeed + strangecorrection) - currentSpeed

	errsum = errsum + err
	k := kp * err
	i := ki * st * errsum
	d := kd * ((err - lasterr) / st)
	log.Printf("PID: k:%f i:%f d:%f ", k, i, d)
	result := stcoeff * (k + i + d)

	//result := currentSpeed + correction

	lasterr = err

	log.Printf("PID: err:%f errsum:%f lasterr:%f result:%f", err, errsum, lasterr, result)

	if result > 1 {
		return 1
	}
	if result < 0 {
		return 0
	}
	return result
}

func detectNewTargetspeed(targetSpeed float64) {
	if targetSpeed != lasttargetspeed {
		lasttargetspeed = targetSpeed
		lasterr = 0
		errsum = 0
	}
}
