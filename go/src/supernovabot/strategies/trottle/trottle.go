package trottle

const (
	MAXACC float64 = 0.2
	TURBOMAX float64 = 0.6
)

func ToTargetSpeed(currentSpeed float64, targetSpeed float64) float64 {
	if currentSpeed < (targetSpeed - MAXACC) {
		return float64(1)
	} else {
		if currentSpeed < targetSpeed {
			return float64(0.5)
		} else {
			return float64(0)
		}
	}
}
