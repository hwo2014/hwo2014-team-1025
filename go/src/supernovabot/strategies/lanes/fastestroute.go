package lanes

import (
	"fmt"
	"log"
	"math"
	"supernovabot/direction"
	"supernovabot/status"
	"supernovabot/targetspeed"
	"supernovabot/track"
)

const (
	maxspeed = 9
)

type RouteElement struct {
	Lane     float64
	Estimate float64
	Index    float64
}

func RouteGetNextSwitch(pieceIndex float64, lane track.Lane, track track.Track, targetspeeds *targetspeed.Targetspeeds, competitors map[string]*status.Status) (int, int) {

	route := calculateRoute(pieceIndex, lane, track, targetspeeds, competitors)
	log.Printf("FINAL RESULT: %s", route)

	_, nextSwitch := track.Get_next_switch(int(pieceIndex))
	afterNextSwitch := getPieceIndex(float64(nextSwitch+1), float64(len(track.GetPieces())))

	var laneIndex float64 = lane.GetIndex()
	for _, element := range route {
		if element.Index == afterNextSwitch {
			laneIndex = element.Lane
			break
		}
	}

	log.Printf("ns: %f  ans: %f li: %f", nextSwitch, afterNextSwitch, laneIndex)

	nextLane := track.GetLane(laneIndex)

	result := direction.NONE

	if nextLane.GetDfc() > lane.GetDfc() {
		result = direction.RIGHT
	}
	if nextLane.GetDfc() < lane.GetDfc() {
		result = direction.LEFT
	}

	return nextSwitch, result
}

func calculateRoute(pieceIndex float64, lane track.Lane, track track.Track, targetspeeds *targetspeed.Targetspeeds, competitors map[string]*status.Status) []RouteElement {
	piecesAmount := len(track.GetPieces())
	var emptyRoute = new([]RouteElement)
	return calculateLanePieceRec(*emptyRoute, lane, pieceIndex, float64(piecesAmount), float64(piecesAmount), 0, track, targetspeeds, 0, competitors)
}

func calculateLanePieceRec(prevRoute []RouteElement, lane track.Lane, currentIndex, depth, piecesAmount, penalty float64, track track.Track, targetspeeds *targetspeed.Targetspeeds, segment int, competitors map[string]*status.Status) []RouteElement {
	_, nextSwitch := track.Get_next_switch(int(currentIndex))

	var route = new([]RouteElement)

	var getMeOutOfHere = true

	//log.Printf("Route: ci:%f ns:%d pa:%f ", currentIndex, nextSwitch, piecesAmount)

	// if we find a competitor in segment 0, w're right behind him. If he goes too slow, we must get out of there!
	if segment == 0 {
		log.Printf("I'm in segment 0 (piece %d lane %d) finding competitors", int(currentIndex), int(lane.GetIndex()))
		for _, status := range competitors {
			if status.Position.PiecePosition.PieceIndex > currentIndex && float64(nextSwitch) > status.Position.PiecePosition.PieceIndex {
				log.Printf("I found someone in segment 0: %s", status.Position.Id.Name)
				if status.Position.PiecePosition.Lane.EndLaneIndex == lane.GetIndex() {
					log.Printf("Oh no, %s is taking the same lane (%d)", status.Position.Id.Name, int(lane.GetIndex()))
					myTargetSpeed := math.Min(targetspeeds.GetTargetSpeed(int(status.Position.PiecePosition.PieceIndex), status.Position.PiecePosition.Lane.EndLaneIndex), maxspeed)
					hisSpeed := status.Speed

					slowdownFactor := hisSpeed / myTargetSpeed

					if slowdownFactor < 0.8 {
						log.Printf("%s is going at %f instead of targetspeed %f (factor: %f), we should get out of here!", status.Position.Id.Name, hisSpeed, myTargetSpeed, slowdownFactor)
						getMeOutOfHere = true
					} else {
						log.Printf("%s is going at %f instead of targetspeed %f (factor: %f), let's stick here for a while", status.Position.Id.Name, hisSpeed, myTargetSpeed, slowdownFactor)
					}

				}

			}

		}

	}

	//If in segment 1, find competitors and use a targetspeed correction if lower than our targetspeed to avoid traffic jams
	var correction = float64(1)
	if segment == 1 {
		log.Printf("I'm in segment 1 (piece %d lane %d) finding competitors", int(currentIndex), int(lane.GetIndex()))
		for _, status := range competitors {
			if status.Position.PiecePosition.PieceIndex > currentIndex && float64(nextSwitch) > status.Position.PiecePosition.PieceIndex {
				log.Printf("I found someone in segment 1: %s", status.Position.Id.Name)
				if status.Position.PiecePosition.Lane.EndLaneIndex == lane.GetIndex() {
					log.Printf("Oh no, %s is taking the same lane (%d)", status.Position.Id.Name, int(lane.GetIndex()))
					myTargetSpeed := math.Min(targetspeeds.GetTargetSpeed(int(status.Position.PiecePosition.PieceIndex), status.Position.PiecePosition.Lane.EndLaneIndex), maxspeed)
					hisSpeed := status.Speed

					if hisSpeed < myTargetSpeed {
						log.Printf("%s drives at %f on piece %d instead of %f, I should take that into a count", status.Position.Id.Name, hisSpeed, int(status.Position.PiecePosition.PieceIndex), myTargetSpeed)

						corr := hisSpeed / myTargetSpeed

						if corr < correction && corr > 0 {
							log.Printf("the old correction was %f so i'm putting %f instead", correction, corr)
							correction = corr
							log.Printf("the new correction is %f", correction, corr)
						}

					}

				}

			}
		}
	}

	for i := currentIndex; i <= float64(nextSwitch); i = getPieceIndex(i+1, piecesAmount) {
		speed := math.Min(targetspeeds.GetTargetSpeed(int(i), lane.GetIndex()), maxspeed) * correction
		length := track.GetPieces()[int(i)].Length(lane.GetDfc())
		ticks := length / speed
		if i == currentIndex {
			ticks = ticks + penalty
		}
		*route = append(*route, RouteElement{lane.GetIndex(), ticks, i})
		// loop fix if last piece is a switch
		if i == float64(nextSwitch) {
			break
		}
	}

	*route = append(prevRoute, *route...)
	//	log.Printf("Route: %s", *route)
	if len(*route) > int(depth) {
		return *route
	}

	shortest := float64(999999)
	var shortestRoute = new([]RouteElement)

	for _, l := range track.GetLanes() {
		if !(getMeOutOfHere && l.GetIndex() == lane.GetIndex()) {
			var result []RouteElement
			if l.GetIndex() != lane.GetIndex() {
				result = calculateLanePieceRec(*route, l, getPieceIndex(float64(nextSwitch)+1, piecesAmount), depth, piecesAmount, cost, track, targetspeeds, segment+1, competitors)
			} else {
				//log.Printf(" Same index DUDE!")
				result = calculateLanePieceRec(*route, l, getPieceIndex(float64(nextSwitch)+1, piecesAmount), depth, piecesAmount, 0, track, targetspeeds, segment+1, competitors)
			}

			//log.Printf("bef -> est:%f shortest:%f ", getTotalEstimate(result), shortest)

			if getTotalEstimate(result) < shortest {
				shortest = getTotalEstimate(result)
				shortestRoute = &result
			}
		}

		//log.Printf("after -> shortest:%f ", shortest)

	}

	//log.Printf("Result Route: %s", *shortestRoute)

	return *shortestRoute
}

func getTotalEstimate(route []RouteElement) (sum float64) {
	for _, routeElement := range route {
		sum = sum + routeElement.Estimate
	}
	return
}

func getPieceIndex(index, piecesAmount float64) float64 {
	return math.Mod(index, piecesAmount)
}

// Look mummy, I have a toString method!
func (r RouteElement) String() string {
	return fmt.Sprintf("{ i:%d l:%d est:%f }", int(r.Index), int(r.Lane), r.Estimate)
}
