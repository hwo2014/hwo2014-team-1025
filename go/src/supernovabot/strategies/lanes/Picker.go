package lanes

import (
	"log"
	"supernovabot/chrono"
	"supernovabot/direction"
	"supernovabot/targetspeed"
	"supernovabot/track"
)

const (
	// it costs 1 tick to switch lanes
	cost = 1
)

type Picker struct {
	track       track.Track
	speeds      *targetspeed.Targetspeeds
	chronometer chrono.Chrono
}

func NewPicker(track track.Track, speeds *targetspeed.Targetspeeds, chronometer chrono.Chrono) Picker {
	return Picker{track, speeds, chronometer}
}

func (picker Picker) GetNextSwitchDirection(piece int, laneindex float64) (int, int) {
	track := picker.track
	currentlane := track.GetLane(laneindex)
	nextSwitch, nextLane := picker.pickLane(piece, laneindex)
	result := direction.NONE

	if nextLane.GetDfc() > currentlane.GetDfc() {
		result = direction.RIGHT
	}
	if nextLane.GetDfc() < currentlane.GetDfc() {
		result = direction.LEFT
	}
	return nextSwitch, result
}

func (picker Picker) pickLane(piece int, laneindex float64) (int, track.Lane) {
	track := picker.track
	currentlane := track.GetLane(laneindex)

	log.Printf("Looking for the best lane after piece %d (currently in lane %d)", piece, int(laneindex))
	_, firstSwitch := track.Get_next_switch(piece + 1)
	_, nextSwitch := track.Get_next_switch(firstSwitch + 1)

	currentticks := picker.ticksForLane(firstSwitch, nextSwitch, currentlane)

	bestlane := currentlane
	for _, lane := range track.GetLanes() {
		ticks := picker.ticksForLane(firstSwitch, nextSwitch, lane)
		if currentticks-ticks > cost {
			log.Printf(" using lane %d in piece %d will bring us down to %f ticks - switching", int(lane.GetIndex()), firstSwitch, ticks)
			bestlane = lane
		} else {
			log.Printf(" using lane %d in piece %d will take %f ticks to get to %d", int(lane.GetIndex()), firstSwitch, ticks, nextSwitch)
		}
	}
	log.Println()
	return firstSwitch, bestlane
}

// TODO: function does not calculate ticks correctly when you're back at the start of the track
// TODO: it would be better to measure the actual number of ticks while driving and use those instead?
func (picker Picker) ticksForLane(start, end int, lane track.Lane) float64 {
	track := picker.track
	speeds := picker.speeds
	//chronometer := picker.chronometer

	total := 0.00
	ticks := 0.00
	for i := start; i <= end; i++ {
		length := track.GetPieces()[i].Length(lane.GetDfc())
		/*
			speed := chronometer.Get(i, int(lane.GetIndex()))
			if speed == chrono.NO_VALUE {
				speed = speeds.GetTargetSpeed(i, lane.GetIndex())
			}*/
		speed := speeds.GetTargetSpeed(i, lane.GetIndex())
		if speed == -1 {
			speed = 10
		}

		if i == start || i == end {
			total = total + length/2
			ticks = ticks + (length / 2 / speed)
		} else {
			total = total + length
			ticks = ticks + (length / speed)
		}
	}

	return ticks
}

//TODO: ensure this class receives status update asynchronously
//      that way, it can keep track of other cars
