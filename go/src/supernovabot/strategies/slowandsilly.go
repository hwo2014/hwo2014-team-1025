package strategies

import (
	"log"
	"math/rand"
	"supernovabot/direction"
	"supernovabot/status"
	"supernovabot/turbo"
)

var currentpiece int = -1

type SlowAndSilly struct {
	throttle float64
}

func NewSlowAndSilly() SlowAndSilly {
	return SlowAndSilly{.30}
}

func (sas SlowAndSilly) Run(status status.Status, turbo *turbo.Turbo) (float64, *string, *string) {
	track := status.Track
	piece := int(status.Position.PiecePosition.PieceIndex)

	var dir *string = nil

	if piece != currentpiece {
		// we ended up in a new piece, let's see if we can act silly just for once now
		currentpiece = piece

		log.Printf("How about we go %s at piece %s?", piece)
		_, switchIndex := track.Get_next_switch(piece)

		if switchIndex == currentpiece+1 {
			dir = direction.ToString(rand.Intn(3))
		}

	}

	return sas.throttle, dir, nil
}
